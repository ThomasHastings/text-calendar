#!/bin/bash
chmod +x xc.py
cp xc.py ~/.local/bin/xc
mkdir -p ~/.local/share/xc

if [[ "$1" == "-u" ]]; then
    rm ~/.local/bin/xc
    rm -r ~/.local/share/xc
fi

if [[ "$1" == "-t" ]]; then
   echo "Installing xc to Termux."
   cp xc.py /data/data/com.termux/files/home/.local/bin/xc
   mkdir -p /data/data/com.termux/files/home/.local/share/xc
fi
