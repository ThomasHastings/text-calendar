# TeXt Calendar (xc)

This tool lets you interact with `calendar.txt` files directly from your terminal, instead of relying on text editors. See the [definition](https://terokarvinen.com/2021/calendar-txt/) for details.

## Installation
The only dependency is Python 3. The `installer.sh` installs it to `~/.local/bin` on conventional UNIX systems. In Termux, run `installer.sh -t`. On Windows, you can run the file directly.

```
git clone https://gitlab.com/ThomasHastings/text-calendar.git
cd text-calendar
# Windows
py xc.py

# UNIX
chmod +x install.sh
./install.sh

 # Termux
chmod +x install.sh
./install.sh -t

# Uninstall
./install.sh -u

```

More intelligent parsing of date formats requires the `dateutil` package:
```
# UNIX
python3 -m pip install python-dateutil

# Windows
py -m pip install python-dateutil
```


## Usage
This is a CLI tool, so all interactions happen in a terminal interface.

Usage from script:
```
python3 xc.py DATE --week --weekly-entry --add --clear --remove --calendarfile
python3 xc.py DATE -w -we -a -c -rm
```

Usage when installed:
```
xc DATE --add --clear --remove --week --calendarfile
xc DATE -a -c -rm -w
```


The `DATE` can be given in a number of formats:
- `today` or `td`
- `tomorrow` or `tm`
- `this_week` or `tw`
- `next_week` or `nw`
- `YYYY-MM-DD` ISO format (e. g., `2023-05-30`)
- `Day` (e. g., `Mon`, `Tue`, etc.)
- `MM-DD` (e. g., `05-30` for 5th of May of the current year)
- `wNN` ISO format (e. g., `w04` for the fourth week of the current year)
- `wNN YYYY` (e. g., `w04 2025` for the fourth week of 2025)

Optional arguments:
- When you call a `DATE` alone, the entry is written to stdout.
- `-w` – Show the whole week for the chosen date.
- `-we` – Interact with (show/edit) the weekly entry corresponding to the chosen date.
- `-a` – Add an entry to that date: `python3 xc.py DATE -a 9:35 Tennis with Eve`
- `-c` – Clear all entries from the chosen date.
- `-rm` – Remove an event from the chosen date. Events have their indexes in brackets to show the number.
- `--calendarfile` – Specify what file to use as `calendar.txt`. Default is `./calendar.txt`

## Examples
### View the schedule for this week

All entries are alphabetically sorted. `HH`, `HHMM` or `HH:MM` time format is recommended.

```
user@homebpython3 xc.py tw
2022-05-30 w22  
   Work on text-calendar (1)
   These are activities for the whole week (2)

2022-05-30 w22 Mon 
   These are daily activities (1)
   Any text can be entered (2)

2022-05-31 w22 Tue 
    
2022-06-01 w22 Wed 
    
2022-06-02 w22 Thu 
    
2022-06-03 w22 Fri 
   07:00 Come up with example activities (1)
   07:30 Write down example activities (2)
   09:35 Hope that they make sense (3)

2022-06-04 w22 Sat 
   20:00 Play video games (1)

2022-06-05 w22 Sun
```

### Add a new entry
```
user@homebpython3 xc.py 2023-09-15 -a Hello to the future
2023-09-15 w37 Fri  
   An extra line to be removed (1)
   Hello to the future (2)

```

### Remove an entry
```
user@homebpython3 xc.py 2023-09-15 -rm 1
2023-09-15 w37 Fri
   Hello to the future (1) 
```

## Why?
I've found most terminal-based calendars either weird or hard to use on mobile devices. The `calendar.txt` concept seems very portable, but editing a file with thousands of lines on a phone felt daunting. This can be simpler, but the file retains its portability.

## Acknowledgment
The idea of calendar.txt comes from [Tero Karvinen](https://terokarvinen.com/2021/calendar-txt/).
