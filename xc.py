#!/usr/bin/python3
import os
import sys
import csv
import argparse
import datetime
import pathlib

def parse_calendar_txt(calendar_file):
    try:
        readfile = open(calendar_file,'r')
        calendar_raw = list(csv.reader(readfile))
        readfile.close()
    except:
        print('No valid calendar.txt found. Exiting')
        exit()
    
    calendar = []
    for line in calendar_raw:
        newline = {}
        
        # Date-related
        line_split = line[0].split('  ')
        date_info = line_split[0].split(' ')
        newline['date'] = datetime.datetime.fromisoformat(date_info[0]).date()
        newline['week'] = date_info[1]
        if len(date_info) > 2:
            newline['weekday'] = date_info[2]
        
        # Events
        newline['events'] = []
        events = line_split[1].split('.')
        for event in events:
            event = event.strip()
            if event != '':
                newline['events'].append(event)
        newline['events'].sort()
        calendar.append(newline)
        # sample entry:
        # {'date': '2022-06-03', 'week': 'w22', 'weekday': 'Fri', 'events': ['07:00 Come up with example activities']}
    
    return calendar

def find(calendar, date=None, year=None, week=None, whole_week=False, match_day=False):
    try:
        for day in calendar:
            if day['date'] == date:
                matched_entry = calendar.index(day)
                break
            
            if day['week'] == week and day['date'].year == year:
                matched_entry = calendar.index(day)
                break
        
        if match_day:
            if "weekday" not in calendar[matched_entry]:
                matched_entry = calendar.index(day)+1 # Given that Monday must come after the weekly entry
                        
        if whole_week == True:
            days_in_week = []
            
            # Find the first line of the given week
            # Matching weeknumber is not enough due to first and last weeks both being w52
            for day in range(matched_entry, matched_entry-8, -1):
                if "weekday" not in calendar[day]:
                    starting_index = day
                    break
              
            # Find the next 8 lines
            for day in range(starting_index,starting_index+8):
                days_in_week.append(day)
            
            return days_in_week
        
        else:            
            return [matched_entry]
    except UnboundLocalError:
        print('Date entry not found.')
        
def parse_input_date(args, weekdays):
    # Set today
    today = datetime.datetime.today()

    # Lowercase days
    weekdays_lower = []
    for day in weekdays:
        weekdays_lower.append(day.lower())
    
    # Start with empty query values
    date = year = week = None    
    
    # Parse query date
    if args['weekly_entry'] == True:
        date=datetime.date.fromisocalendar(today.year, today.isocalendar()[1], 1)
        
    for query in args['date']:
        if query in ['td', 'today']:
            date = today
            date = date.date()
        
        if query in ['tm', 'tomorrow']:
            date = today + datetime.timedelta(days=1)
            date = date.date()
        
        if query in ['tw', 'this_week']:
            week = 'w'+str(today.isocalendar().week)
        
        if query in ['nw', 'next_week']:
            week = 'w'+str((today+datetime.timedelta(weeks=1)).isocalendar().week)
        
        if str(query)[0] == 'w':
            weeknumber = query.replace('w','')
            week = 'w'+str(weeknumber).zfill(2)
        
        if query.isdigit():
            if 1900 < int(query) < 2100:
                year = int(query)
        
        if len(str(query)) == 5:
            if len(query.split('-')) == 2:
                year = today.year
                date = datetime.datetime.fromisoformat(str(year)+"-"+query).date()
        
        if str(query).lower() in weekdays_lower:
            if today.weekday() == 6 or weekdays_lower.index(query) <= today.weekday():
                date=datetime.date.fromisocalendar(today.year, today.isocalendar()[1]+1, weekdays_lower.index(query)+1)      
            else:
                date=datetime.date.fromisocalendar(today.year, today.isocalendar()[1]+1, weekdays_lower.index(query)+1)    

        try:
            parsed_date = datetime.datetime.fromisoformat(query).date()
            
            if week is not None:
                year = parsed_date.year
                
            if date is None:
                date = parsed_date
            
        except:
            pass     
        
    if year == None:
        year = today.year
        
    return date, year, week
    

def format_preview(line):
    # Recreate original formatting for the date
    printline = str(line['date'].isoformat()) +' '+line['week']
    if 'weekday' in line and line['date']:
        printline = printline + ' ' + line['weekday'] + '  \n'
    else:
        printline = printline + '  \n'
    
    # Add each event to a new line
    for event in line['events']:
        printline = printline + '   ' + str(event) + ' (' + str(line['events'].index(event)+1) + ')\n'
    return printline
    
def format_output(line):
    # Recreate original formatting for the date 
    outline = str(line['date'].isoformat()) +' '+line['week']
    if 'weekday' in line:
        outline = outline + ' ' + line['weekday'] + '  '
    else:
        outline = outline + '  '
    
    # Add events with '.' separation
    for event in line['events']:
        outline = outline + event + '. '
    
    return outline

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)

def generate_calendar(filepath, weekdays, years_to_cover=10):
    calendar_file=filepath
    start_year = datetime.datetime.today().year
    end_year =  (datetime.datetime.today() + datetime.timedelta(weeks=years_to_cover*52)).year
    start_date = datetime.date(start_year, 1, 1)
    end_date = datetime.date(end_year+1, 1, 1)

    dates_list = []
    for single_date in daterange(start_date, end_date):
        dates_list.append({'date': single_date,
                           'week': 'w'+str(single_date.isocalendar()[1]).zfill(2),
                           'weekday': weekdays[single_date.isocalendar()[2]-1],
                           'events': []})

    outfile=open(calendar_file, 'w')
    for date in dates_list:
        if date['weekday'] == weekdays[0]:
            outfile.write(format_output({'date': date['date'], 'week': date['week'], 'events': []})+'\n')
        outfile.write(format_output(date)+'\n')
    outfile.close()
    print(calendar_file, 'was created.')


def main():
    # Localization is possible here, but then the locale must match that of the calendar.txt in use
    weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    
    # Construct an argument parser
    all_args = argparse.ArgumentParser(epilog='Possible formats for the query date: [YYYY-MM-DD | MM-DD] [YYYY wWW | wWW] [today | td] [tomorrow | tm] [this_week | tw] [next_week | nw]')

    # Add arguments to the parser
    all_args.add_argument("date", nargs='+',
                          help='query date.')
    all_args.add_argument("-a", "--add", nargs='+', required=False,
                          help='Add an event to a date.')
    all_args.add_argument("-rm", "--remove", nargs='+', required=False, type=int,
                           help='Remove an event from a date based on its index shown in brackets next to it (e. g., "1").')
    all_args.add_argument("-c", "--clear", action='store_true', required=False,
                      help='Clear all events from a date.')
    all_args.add_argument("-w", "--week", action='store_true', required=False,
                      help='View whole week.')
    all_args.add_argument("-we", "--weekly-entry", action='store_true', required=False,
                      help='Interact with the weekly entry for the given week.')
    all_args.add_argument("--calendarfile", required=False, default='calendar.txt',
                      help='Specify a calendar.txt file to use.')
    args = vars(all_args.parse_args())
    
    # Import calendar file
    if sys.argv[0][-2:] != 'xc':
        calendar_file = args['calendarfile']
    else:
        calendar_file = (pathlib.Path.home() / '.local' / 'share' / 'xc' / 'calendar.txt')

    if os.path.exists(calendar_file) is False:
        generate_calendar(calendar_file, weekdays)
        
    calendar = parse_calendar_txt(calendar_file)
    
    # Create usable coordinates from input
    date, year, week = parse_input_date(args, weekdays)
    
    # Shorthands tw and nw show the full weeks
    if args['date'][0] == 'tw' or args['date'][0] == 'nw':
        args['week'] = True
    
    for arg in args['date']:
        if arg[0] == 'w':
            args['week'] = True
    
    # Find the coordinates in the calendar
    if args['weekly_entry']:
        match_day = False
    else:
        match_day = True
    
    query_lines = find(calendar, date=date, week=week, year=year, whole_week=args['week'], match_day=match_day)

    # Make changes to calendar if needed
    if query_lines is not None:        
        if args['add'] is not None or args['remove'] is not None or args['clear']:
            target_line = calendar[query_lines[0]]
            
            # Add new event
            if args['add'] is not None:
                entryline = ''
                for entry in range(len(args['add'])):
                    entryline = entryline + str(args['add'][entry]) + ' '
                entryline = entryline.strip()
                target_line['events'].append(entryline)
            
            # Remove event
            if args['remove'] is not None:
                for remove_arg in args['remove']:
                    if type(remove_arg) == int:
                        try:
                            del(target_line['events'][remove_arg-1])
                        except IndexError:
                            print('Entry not found, nothing was removed')
            
            # Clear day
            if args['clear']:
                target_line['events'] = []
            
            # Rewrite file with modifications
            outfile = open(calendar_file,'w')
            for line in range(len(calendar)):
                if line == query_lines[0]:
                    outfile.write(format_output(target_line) + '\n')
                else:
                    outfile.write(format_output(calendar[line]) + '\n')
            outfile.close()
            
            print(format_preview(target_line))
            
        else:
            printlines = []
            for line in query_lines:
                printlines.append(format_preview(calendar[line]))
            
            for line in printlines:
                print(line)
                
if __name__ == '__main__':
    main()

